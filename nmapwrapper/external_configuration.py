from configparser import ConfigParser, ExtendedInterpolation

default_config_data = None

def read_config( file_name = "/etc/nmapwrapper/nmapwrapper.conf" ):
    config = ConfigParser(  interpolation = ExtendedInterpolation() ) 

    config.read( file_name )

    is_daemon = config.getboolean( "default", "is-daemon" )
    pid = config.get( "default", "pid" )
    
    agent_host = config.get( "default", "agent-host" )
    listening_port = config.getint( "default", "listening-port" )

    log_filename = config.get( "default", "log-filename" )
    log_level = config.get( "default", "log-level" )
    msg_format = config.get( "default", "msg-format" )
    max_bytes = config.getint( "default", "max-bytes" )
    backup_count = config.getint( "default", "backup-count" )

    scan_network_cidr = config.get( "default", "scan-network-cidr" )

    whitelist_remote_requests = config.get( "default", "whitelist-remote-requests" )

    global default_config_data
    default_config_data = {
        "is-daemon": is_daemon,
        "pid": pid,
        "agent-host": agent_host,
        "listening-port": listening_port,
        "log-filename": log_filename,
        "log-level": log_level,
        "msg-format": msg_format,
        "max-bytes": max_bytes,
        "backup-count": backup_count,
        "scan-network-cdir": scan_network_cidr,
        "whitelist-remote-requests": whitelist_remote_requests,
    }

def get_default_section():
    global default_config_data    
    return default_config_data 


