import datetime 

from .constants import NMAP_VERSION_COMMAND
from .nmap_command import NMapCommandInSubprocess

from .nmap_wrapper import logger

IMPORTANT_LINE = "Host:"

def nmap_version():
    command_base = NMAP_VERSION_COMMAND
    command_all = command_base

    # Run Linux Command in Subprocess
    start = datetime.datetime.now()
    logger.debug("***** Ready to Send Torpedo to Target Board ... %s @ %s !!!", command_all, start) 

    nmap_command = NMapCommandInSubprocess(command_all)
    nmap_command.run_linux_command_blocking()
    
    # NMAP Response .. utf-8
    whole_response_string_utf8 = str(nmap_command)

    return whole_response_string_utf8

    # Return NMAP Response ... Total   
    #return whole_response_string_utf8
