from tcpclientserver.server import prepare_agent_for_remote_requests

from nmapwrapper.nmap_actions import NMAPActions
from .external_configuration import read_config, get_default_section
from .local_logger import log_setup, get_logger
        
#from . import logger
logger = None 

class NMapWrapper(object):
    # ##
    # Save New Request for Handling
    def __init__(self, request):
        logger.debug("Inside NMapWrapper ... Ready to Handle New Request \"%s\" !", request)
        self.request = request
    
    # ##
    # Parse string and find {request-type, net-object, required-action} ... ex nmap:host:discovery
    def to_parse( self ):
        # Check requests beginning 
        if not self.request.startswith("nmap:"):
            raise Exception("Unknown Request Type ... Can Handle Only nmap: !")

        # Check request target & action
        t = self.request.split(":")
        if len(t) > 5:
            raise Exception("Uknown Request Format ... Can handle Only nmap:host:discovery")

        self.tool   = t[0]
        self.target = t[1]
        self.action = t[2]

        if len(t) > 3:
            self.server = t[3]

        # ends with !
        if self.action.endswith("!"):
            # Remove exclamation mark
            self.action = self.action[:-1]

        # if self.key.endswith("port:scan!"):
        #    logger.debug("Have a PORT-SCAN request for NMAP")
        #    self.nmap_open_ports()        
    
    # ##
    # Call right tool for requested target & action
    def call_right_tool(self):
        if self.tool.lower() != "nmap":
            raise Exception("Call Right Tool Error ... ABORTING !")

        # This Agent is only responsible for NMAP network-tool
        #logger.debug("++++++ target = %s , %s, %s", self.target, self.action, self.server)
        
        if self.target.lower() == "host":
            return NMAPActions( target = self.target, action = self.action )
        elif self.target.lower() == "port":
            return NMAPActions( target = self.target, action = self.action, server = self.server )
        elif self.target.lower() == "ping" and self.action.lower() == "pong":
            return NMAPActions( target = "ping", action = "pong")

        
    # ##
    # Handle New Request    
    def handle_new_request(self):
        # Step 1 --> Parse the long-string of request from client to server 
        self.to_parse()

        # Step 2 --> Prepare appropriate tool for target & action
        tool = self.call_right_tool()

        # Step 3 --> Call selected tool to execute requested action on specified target object
        targets_found_str = tool.call_nmap_target_action()

        #logger.debug("Targets Found by NMAP ... %s", targets_found_str)

        return targets_found_str
 
########
#### 
def main():
    read_config()
    default_section = get_default_section()
    
    log_filename = default_section[ "log-filename" ]
    log_level = default_section[ "log-level" ]
    msg_format = default_section[ "msg-format" ]
    max_bytes = default_section[ "max-bytes" ]
    backup_count = default_section[ "backup-count" ]

    log_setup( filename = log_filename, level = log_level, msg_format = msg_format, maxBytes = max_bytes, backupCount = backup_count )

    global logger
    logger = get_logger()

    logger.debug("NMap-Wrapper Ready to Start !")
    
    is_daemon = default_section["is-daemon"]

    agent_host = default_section["agent-host"]
    listening_port = default_section["listening-port"] 

    whitelist_remote_requests = default_section["whitelist-remote-requests"]
    whitelist_arr = whitelist_remote_requests.split( "," )
    logger.debug( "Whitelist for remote requests is +++ %s", whitelist_arr )

    if is_daemon:
        # Run the NMAP-Wrapper as a Linux Daemon
        pid = default_section[ "pid" ]

        logger.debug( "\n" +
            "********************************************************************\n" + 
            "Ready to Run the NMAP-Wrapper Application as a Linux Daemon with PID ... %s !\n" + 
            "********************************************************************", pid)
    else: 
        logger.debug( "Ready to Run the NMAP-Wrapper as a Standalone Application !" )
        prepare_agent_for_remote_requests( logger, host = agent_host, port = listening_port, whitelist = whitelist_arr )
